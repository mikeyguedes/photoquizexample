package ca.abodzay.photoquiz

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {
    private lateinit var quiz: Quiz
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val answers = resources.getStringArray(R.array.answers).toCollection(ArrayList())
        val wrongAnswers = resources.getStringArray(R.array.wrong_answers).toCollection(ArrayList())
        //mixing correct answers into wrong answers to make quiz more challenging
        wrongAnswers.addAll(answers)
        val images = intArrayOf(
            R.drawable.image_0,
            R.drawable.image_1,
            R.drawable.image_2,
            R.drawable.image_3,
            R.drawable.image_4,
            R.drawable.image_5,
            R.drawable.image_6,
            R.drawable.image_7,
            R.drawable.image_8,
            R.drawable.image_9
        ).toCollection(ArrayList())
        quiz = Quiz(answers, images, wrongAnswers)
        if(savedInstanceState != null){
            //load question
            val loadedAnswers = savedInstanceState.getStringArrayList("allAnswers") as ArrayList<String>
            val loadedCorrect = savedInstanceState.getString("correctAnswer") as String
            val loadedImage = savedInstanceState.getInt("answerImage")
            val loadedQuestion :Question = Question(loadedAnswers, loadedImage, loadedCorrect)
            quiz.setQuestion(loadedQuestion)

            //load scores
            quiz.streak = savedInstanceState.getInt("streak")
            quiz.highScore = savedInstanceState.getInt("highScore")
        }else{
            //if no bundle, load high score from preferences
            val sharedPref = getPreferences(Context.MODE_PRIVATE)
            quiz.highScore = sharedPref.getInt("highScore", 0)
        }

        setContentView(R.layout.activity_main)
        setQuizUI()
    }

    //sets the UI to reflect the current state of the quiz
    fun setQuizUI(){
        findViewById<Button>(R.id.button1).text = quiz.getAnswerText(0)
        findViewById<Button>(R.id.button2).text = quiz.getAnswerText(1)
        findViewById<Button>(R.id.button3).text = quiz.getAnswerText(2)
        findViewById<Button>(R.id.button4).text = quiz.getAnswerText(3)

        findViewById<TextView>(R.id.streak_view).text = getString(R.string.streak) + quiz.streak
        findViewById<TextView>(R.id.high_score_view).text = getString(R.string.high_score) + quiz.highScore

        findViewById<ImageView>(R.id.answerImage).setImageResource(quiz.getImageRes())
    }

    //OnClick for when the user selects an answer. Pops up a toast based on whether it was right or not.
    fun answerButtonClick(view: View) {
        val button = view as Button
        Log.i("BUTTON", "User answered" + button.text)
        if(quiz.checkAnswer(button.text as String)){
            Toast.makeText(this, R.string.right_toast, Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(this, R.string.wrong_toast, Toast.LENGTH_SHORT).show()
        }
        setQuizUI()
    }

    //OnClick for reset button
    fun resetButtonClick(view: View){
        Log.i("BUTTON", "User hit reset button")
        quiz.resetHighScore()
        setQuizUI()
    }


    // invoked when the activity may be temporarily destroyed
    override fun onSaveInstanceState(outState: Bundle) {
        outState?.run {
            //save question
            putStringArrayList("allAnswers", quiz.getQuestion().qAnswers)
            putInt("answerImage", quiz.getQuestion().qImage)
            putString("correctAnswer",quiz.getQuestion().correctAnswer)
            //Save scores
            putInt("streak", quiz.streak)
            putInt("highScore", quiz.highScore)
        }
        super.onSaveInstanceState(outState)
    }


    //When the activity stops, we want to save the high score to shared preferences
    override fun onStop() {
        super.onStop()

        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putInt("highScore",quiz.highScore)
        editor.apply()

    }

}